
This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/ECEDBC433379CF13E2534C29B5B0AA7ED4509C8B) to this [GitLab account](https://gitlab.com/steinea). For details check out <https://docs.keyoxide.org/service-providers/gitlab/>

[Verifying my OpenPGP key: openpgp4fpr:ECEDBC433379CF13E2534C29B5B0AA7ED4509C8B]
